class BaseCrawler:
    """
    General class to serve as baseline for the other crawler classes
    """
    def __init__(self):
        # FIXME - initialise logger propertly
        self.logger = None
        self.objects = []