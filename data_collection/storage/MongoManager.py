import pymongo
import logging

import LogHandler
from ConfigHandler import ConfigHandler
from data_collection.DataEnums import DataEnums


class MongoManager():
    def __init__(self):
        self.logger = LogHandler.LogHandler.create_logger(__name__)
        self.config = ConfigHandler()
        self.hostname = self.config.get_value(DataEnums.MONGO_SECTION.value, 'hostname')
        self.port = int(self.config.get_value(DataEnums.MONGO_SECTION.value, 'port'))
        self.database_name = self.config.get_value(DataEnums.MONGO_SECTION.value, 'database')
        self.collection_name = self.config.get_value(DataEnums.MONGO_SECTION.value, 'collection')
        self.username = self.config.get_value(DataEnums.MONGO_SECTION.value, 'username')
        self.password = self.config.get_value(DataEnums.MONGO_SECTION.value, 'password')

    def start_connection(self):

        self.logger.info("Starting connection to mongo")
        connection = pymongo.MongoClient(self.hostname, self.port)
        db = connection[self.database_name]
        db.authenticate(self.username, self.password)
        self.collection = db.get_collection(self.collection_name)

    def insert_dataframe(self, df) -> None:

        self.logger.info("Inserting data into mongo")
        self.logger.debug("Data to be inserted: {}".format(str(df.head())))

        self.collection.insert_many(df.to_dict('records'))