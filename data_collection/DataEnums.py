from enum import Enum


class DataEnums(Enum):

    # Reddit Crawling
    ETH_TRADER = 'ethtrader'
    DEFAULT_USER_AGENT = 'python.config: /r/wisk123'
    REDDIT_CONFIG_SECTION_NAME = 'reddit'

    #Mongo storage
    MONGO_SECTION="mongo"