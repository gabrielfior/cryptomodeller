import datetime

import pandas as pd

class RedditAnalyzer:
    """
    Analyzer of data fetched from reddit
    """
    def __init__(self, comments_map : dict):
        self.comments_map = comments_map
        self.dataframe = None

    def organize_into_dataframe(self):

        entries = []
        # organize submissions and comments into list of dicts
        for submission_id, item in self.comments_map.items():

            entries.append({'type': 'submission',
                            'subreddit': item['subreddit_name'],
                            'id': submission_id,
                            'description': item['submission'].title,
                            'created_utc': item['submission'].created_utc,
                            'num_comments': item['submission'].num_comments})

            for comment in item['comments']:
                entries.append({
                    'type': 'comment',
                    'id': comment.id,
                    'description': comment.body,
                    'created_utc' : comment.created_utc
                })

        # return grouped dataframe
        return self.group_dataframe(pd.DataFrame(entries))

    def group_dataframe(self, df):
        df["day"] = df.created_utc.apply(
            lambda x: datetime.datetime.combine(datetime.datetime.fromtimestamp(x).date(),
                                                datetime.datetime.min.time()))

        return pd.DataFrame(df.groupby(['day', 'type']).agg({'id': 'count'}).to_records())

