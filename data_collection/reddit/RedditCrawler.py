import datetime
from typing import overload

import praw

from data_collection.BaseCrawler import BaseCrawler


class RedditCrawler(BaseCrawler):
    """
    Crawls data from Reddit
    """
    def __init__(self, reddit_config, **kwargs):
        super().__init__(**kwargs)
        self.reddit_config = reddit_config

        # FIXME - read from config.ini file
        self.reddit = praw.Reddit(client_id=reddit_config.client_id, client_secret=reddit_config.client_secret,
                             user_agent=reddit_config.user_agent)

        #os.environ["APPDATA"] = "C:\\Users\\d91421\\PycharmProjects\\CryptoModeller\\data\\resources"
        # reddit = praw.Reddit('wisk123',user_agent='python:cryptomodeller:v0.0.1 (by /u/wisk123).')

    def open_subreddit(self, display_name):
        '''
        Creates subreddit object
        :param Display_name: name of subreddit
        :return: Subreddit object
        '''
        return self.reddit.subreddit(display_name)

    @overload
    def retrieve_submissions_from_subreddit(self, number_submissions, subreddit, subreddit_adress):


        submission = self.reddit.submission(url=subreddit_adress)
        return self.fetch_submissions(submission)

    @overload
    def retrieve_submissions(self, number_submissions, subreddit_id):

        submission = self.reddit.submission(id=subreddit_id)
        return self.fetch_submissions(submission)

    def fetch_submissions(self, subreddit, limit_number_of_submissions = None):
        '''
        Fetches hottest submissions from subreddit
        :param limit_number_of_submissions: Maximum number of submissions that should be retrieved
        :param subreddit:
        :return:
        '''

        submissions = [i for i in subreddit.hot(limit=limit_number_of_submissions)]
        return submissions

    def retrieve_comments_from_submission(self, submission, limit = None):
        '''
        Retrieves all (or most of) the comments from a submission
        :param submission: Submission object
        :return: List of comments
        '''

        comments = []
        submission.comments.replace_more(limit=limit)
        for comment in submission.comments.list():
            comments.append(comment)

        return comments

    def fetch_creation_datetime_from_submission(self, submission):
        '''
        Retrieves creation date and time of submission in subreddit
        :param submission: Submission object
        :return: Datetime object with creation time
        '''
        return datetime.datetime.fromtimestamp(submission.created_utc)

    def fetch_useful_info_from_submission(self, submission):
        return {'ups': submission.ups, 'downs': submission.downs, 'score': submission.score,
                'num_comments' : submission.num_comments, 'num_crossposts': submission.num_crossposts,
                'comment_limit': submission.comment_limit}

    def fetch_useful_info_from_subreddit(self, subreddit):
        return {'subscribers': subreddit.subscribers,
                'subreddit_creation_time': datetime.datetime.fromtimestamp(subreddit.created_utc),
                'active_user_count': subreddit.active_user_count,
                'accounts_active':subreddit.accounts_active}

#list_threads = reddit.subreddit('learnpython').hot(limit=100)

  # assume you have a Reddit instance bound to variable `reddit`
