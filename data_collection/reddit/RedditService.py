from LogHandler import LogHandler
from data_collection.reddit.RedditConfig import RedditConfig
from data_collection.reddit.RedditCrawler import RedditCrawler
import logging

class RedditService:
    def __init__(self):
        self.logger = LogHandler.create_logger(__name__)
        reddit_config = RedditConfig()

        self.reddit_crawler = RedditCrawler(reddit_config)

    def fetch_all_comments_from_subreddit(self, display_name, limit_submissions=None, limit_comments=None):
        self.logger.info("Fetching comments from subreddit")
        subreddit = self.reddit_crawler.open_subreddit(display_name)
        submissions = self.reddit_crawler.fetch_submissions(subreddit, limit_submissions)

        comments_map = {}
        for sub in submissions:
            comments_map[sub.id] = {
                'comments': self.reddit_crawler.retrieve_comments_from_submission(sub, limit_comments),
                'submission': sub,
                'subreddit_name': display_name}

        return comments_map
