"""
Retrieves data from several subreddits and stores in DB
"""

from data_collection.DataEnums import DataEnums
from data_collection.reddit.RedditAnalyzer import RedditAnalyzer
from data_collection.reddit.RedditService import RedditService
from data_collection.storage.MongoManager import MongoManager

if __name__== "__main__":

    # Retrieve data from Reddit

    reddit_service = RedditService()
    comments = reddit_service.fetch_all_comments_from_subreddit(display_name = DataEnums.ETH_TRADER.value,
                                                          limit_submissions=20,
                                                          limit_comments=None)

    # analysis
    analyzer = RedditAnalyzer(comments)
    df = analyzer.organize_into_dataframe()

    # export to local csv file
    df.to_csv("local_reddit_data.csv")

    # TODO - store data from only last day or last 2 days (filter data)
    # export to mongo
    mongo_manager = MongoManager()
    mongo_manager.start_connection()
    mongo_manager.insert_dataframe(df)
