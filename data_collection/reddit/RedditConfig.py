
from ConfigHandler import ConfigHandler
from LogHandler import LogHandler
from data_collection.DataEnums import DataEnums


class RedditConfig():
    def __init__(self):
        self.logger = LogHandler.create_logger(__name__)

        self.config = ConfigHandler()

        self.client_id = self.config.get_value(DataEnums.REDDIT_CONFIG_SECTION_NAME.value, 'client_id')
        self.client_secret = self.config.get_value(DataEnums.REDDIT_CONFIG_SECTION_NAME.value, 'client_secret')
        self.password = self.config.get_value(DataEnums.REDDIT_CONFIG_SECTION_NAME.value, 'password')
        self.user_agent = DataEnums.DEFAULT_USER_AGENT.value
