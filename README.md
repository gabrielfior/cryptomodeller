# CryptoModeller

Python package for modelling cryptocurrencies price trends.

## Authors

* **Gabriel Fior** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

