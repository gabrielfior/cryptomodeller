

import random

from bokeh.layouts import widgetbox, row
from bokeh.models import HoverTool, FactorRange, Plot, LinearAxis, Grid, Range1d, TextInput, Slider, Select
from bokeh.io import curdoc
from bokeh.models.glyphs import VBar
from bokeh.models.sources import ColumnDataSource
from flask import Flask, render_template, request

from bokeh.embed import components
from bokeh.plotting import figure
from bokeh.resources import INLINE
import pandas as pd


app = Flask(__name__)

iris_df = pd.read_csv('data/iris.csv')

def create_hover_tool():
    """Generates the HTML for the Bokeh's hover data tool on our graph."""
    hover_html = """
          <div>
            <span class="hover-tooltip">$x</span>
          </div>
          <div>
            <span class="hover-tooltip">@bugs bugs</span>
          </div>
          <div>
            <span class="hover-tooltip">$@costs{0.00}</span>
          </div>
        """
    return HoverTool(tooltips=hover_html)


def create_bar_chart(data, title, x_name, y_name, hover_tool=None,
                     width=1200, height=300):
    """Creates a bar chart plot with the exact styling for the centcom
       dashboard. Pass in data as a dictionary, desired plot title,
       name of x axis, y axis and the hover tool HTML.
    """
    source = ColumnDataSource(data)
    xdr = FactorRange(factors=data[x_name])
    ydr = Range1d(start=0,end=max(data[y_name])*1.5)

    tools = []
    if hover_tool:
        tools = [hover_tool,]

    plot = figure(title=title, x_range=xdr, y_range=ydr, plot_width=width,
                  plot_height=height, h_symmetry=False, v_symmetry=False,
                  min_border=0, toolbar_location="above", tools=tools,
                  responsive=True, outline_line_color="#666666")

    glyph = VBar(x=x_name, top=y_name, bottom=0, width=.8,
                 fill_color="#e12127")
    plot.add_glyph(source, glyph)

    xaxis = LinearAxis()
    yaxis = LinearAxis()

    plot.add_layout(Grid(dimension=0, ticker=xaxis.ticker))
    plot.add_layout(Grid(dimension=1, ticker=yaxis.ticker))
    plot.toolbar.logo = None
    plot.min_border_top = 0
    plot.xgrid.grid_line_color = None
    plot.ygrid.grid_line_color = "#999999"
    plot.yaxis.axis_label = "Bugs found"
    plot.ygrid.grid_line_alpha = 0.1
    plot.xaxis.axis_label = "Days after app deployment"
    plot.xaxis.major_label_orientation = 1
    return plot

@app.route("/bars/<int:bars_count>/")
def chart(bars_count):
    if bars_count <= 0:
        bars_count = 1

    data = {"days": [], "bugs": [], "costs": []}
    for i in range(1, bars_count + 1):
        data['days'].append(i)
        data['bugs'].append(random.randint(1, 100))
        data['costs'].append(random.uniform(1.00, 1000.00))

    hover = create_hover_tool()
    plot = create_bar_chart(data, "Bugs found per day", "days",
                            "bugs", hover)
    script, div = components(plot)

    return render_template("chart.html", bars_count=bars_count,
                           the_div=div, the_script=script)



def create_figure(feature_name):

    #fruits = iris_df.columns[0:-1]
    #p = figure(x_range=fruits, plot_height=250, title="Fruit Counts",
    #           toolbar_location=None, tools="")
    #p.vbar(x_range=fruits, top=[5, 3, 4, 2, 4, 6, random.random()*7], width=0.9)
    #return p

    plot = figure(plot_height=400, plot_width=400, title="Order total by customer age",
                  tools="crosshair,pan,reset,save,wheel_zoom",
                  x_range=[0, 10], y_range=[0, 10])

    x = iris_df['petal_width']
    y = iris_df['petal_length']
    source = ColumnDataSource(data=dict(x=x, y=y))

    plot.scatter('x', 'y', source=source)

    # Set up layouts and add to document
    inputs = widgetbox()
    curdoc().add_root(row(inputs, plot, width=800))

    # Set up widgets
    text = TextInput(title="Title", value='Order total by customer age')
    min_year = Slider(title="Order year start", value=2013, start=2013, end=2017, step=1)
    max_year = Slider(title="Order year max", value=2017, start=2013, end=2017, step=1)
    category = Select(title="T-shirt Category", value="All",
                      options=[("All", "All"), ("White T-Shirt M", "White T-Shirt M"),
                               ("White T-Shirt F", "White T-Shirt F"), ("Black T-Shirt M", "Black T-Shirt M"),
                               ("Black T-Shirt F", "Black T-Shirt F"), ("Hoodie", "Hoodie"),
                               ("Tennis Shirt", "Tennis Shirt")])

    def update_title(attrname, old, new):
        plot.title.text = text.value

    def update_data(attrname, old, new):
        category_value = category.value
        selected = iris_df[(iris_df.order_date_year>=min_year.value) & (iris_df.order_date_year<=max_year.value)]
        if (category_value != "All"):
            selected = selected[selected.tshirt_category.str.contains(category_value)==True]
        # Generate the new plot
        x = selected['petal_width']
        y = selected['peta_length']
        source.data = dict(x=x, y=y)


    # Set up callbacks
    text.on_change('value', update_title)

    for w in [min_year, max_year, category]:
        w.on_change('value', update_data)

    return plot


@app.route('/')
def bokeh():

    feature_name1 = request.args.get("feature_name")

    if feature_name1 == None:
        feature_name1 = "Sepal Length"


    fig = create_figure(feature_name1)

    # grab the static resources
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()

    # render template
    script, div = components(fig)

    return render_template(
        'index.html',
        plot_script=script,
        plot_div=div,
        js_resources=js_resources,
        css_resources=css_resources,
        feature_names=['test1','test2','test3'])

if __name__ == "__main__":
    app.run(debug=True)