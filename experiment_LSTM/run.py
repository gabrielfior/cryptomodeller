import experiment_LSTM.ltsm as lstm
import time
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error, mean_squared_error

def plot_results(predicted_data, true_data):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')
    plt.legend()
    plt.show()


def plot_results_multiple(predicted_data, true_data, prediction_len):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    # Pad the list of predictions to shift it in the graph to it's correct start
    for i, data in enumerate(predicted_data):
        padding = [None for p in range(i * prediction_len)]
        #plt.plot(padding + data, label='Prediction')


    list_all_predictions = [item for sublist in predicted_data for item in sublist]
    ax.plot(list_all_predictions, color='red',label='all_predictions')
    plt.legend()
    plt.title("ETH price modelling (unscaled)")
    plt.xlabel("Days")
    plt.show()


# Main Run Thread
if __name__ == '__main__':
    global_start_time = time.time()
    epochs = 20
    window_size = 5

    print('> Loading data... ')

    filename = "/Users/gabrielfior/CryptoModeller/notebooks/data/ETH.csv"
    X_train, y_train, X_test, y_test, scaler = lstm.load_data(filename, window_size, False, 'close')

    print('> Data Loaded. Compiling...')

    model = lstm.build_model([1, window_size, 100, 1])

    model.fit(
        X_train,
        y_train,
        batch_size=512,
        nb_epoch=epochs,
        validation_split=0.05)

    predictions = lstm.predict_sequences_multiple(model, X_test, window_size, window_size)
    # predicted = lstm.predict_sequence_full(model, X_test, seq_len)
    # predicted = lstm.predict_point_by_point(model, X_test)

    print('Training duration (s) : ', time.time() - global_start_time)
    plot_results_multiple(predictions, y_test, window_size)
    list_all_predictions = [item for sublist in predictions for item in sublist]
    mse = mean_squared_error(list_all_predictions, y_test)
    mae = mean_absolute_error(list_all_predictions, y_test)
    print("MSE : {}, MAE : {}".format(mse, mae))