from enum import Enum


class ConfigEnums(Enum):

    # Config filename
    CONFIG_FILEPATH = 'config/config.ini'