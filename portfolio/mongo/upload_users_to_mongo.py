from portfolio.mongo import user
from portfolio.mongo.UserSchema import UserSchema
import mongoengine
from marshmallow import Schema, fields, pprint

mongoengine.connect('CryptoModeller', host='localhost', port=27017)

user_schema = UserSchema()
u, errors = user_schema.load({
     "name": "John Doe", "email": "jdoe@example.com", "password": "123456",
     "tasks": [{"content": "Find a proper password"}]})
u.save()
#<User: User object>
u.name
#"John Doe"
u.tasks
user_schema.dump(u)