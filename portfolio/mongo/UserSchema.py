import marshmallow_mongoengine as ma

from portfolio.mongo.user import User


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User