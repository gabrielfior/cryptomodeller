import json
import random
import datetime

import numpy
import numpy as np
import os

from tqdm import trange

from portfolio.model.CoinStatisticalData import CoinStatisticalData
from portfolio.Forecaster import Forecaster

import matplotlib

from portfolio.model.MonteCarloResult import MonteCarloResult, MonteCarloEncoder

matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
from LogHandler import LogHandler

def divide_by_total_weight(x, total_weight):
    return x/total_weight

class MonteCarlo:

    def __init__(self):
        self.logger = LogHandler().create_logger()

    def randomly_allocate_assets(self, list_coins):
        """
        Creates asset allocation for list of coins
        :param list_coins: Coins to which weights should be calculated.
        :return:
        """
        asset_allocation = {}
        for coin in list_coins:
             asset_allocation[coin] = random.random()

        # sum weights, allocate proportions
        total_prob = np.array(list(asset_allocation.values())).sum()
        asset_allocation = {k: divide_by_total_weight(v, total_prob) for k, v in asset_allocation.items()}

        return asset_allocation

    def run_scenarios(self, number_scenarios, list_coins, forecasts, standard_deviation_of_returns, correlation):
        """
        Run Monte Carlo simulation
        :param number_scenarios: Number of simulations that should be run
        :param list_coins: List of coins to be allocated
        :param predictions: Dict : coin -> [yhat]
        :param standard_deviation_of_returns: Standard deviation of returns until now -> std[coin] = standard_dev
        :param correlation: Correlation coefficient between coins : dict -> dict[coinA][coinB] = rho_A_B
        :return:
        """

        scenarios = []
        for sim_number in trange(number_scenarios):

            variance = 0.
            total_return = 0.

            # distribute assets
            asset_allocation = self.randomly_allocate_assets(list_coins)

            for coin in list_coins:

                stats = CoinStatisticalData(coin, 
                                            asset_allocation, 
                                            forecasts,
                                            standard_deviation_of_returns, 
                                            correlation).to_dict()
                
                weight = stats['weight']
                forecast = stats['forecast']
                standard_dev = stats['standard_dev']
                correlation_indices = stats['correlation_indices']

                
                total_return += forecast['yhat']*weight
                variance += self.calculate_coin_variance(weight, standard_dev)

                for other_coin in list_coins:
                    if other_coin == coin:
                        continue

                    covariance = correlation_indices['close_' + other_coin]

                    other_coin_stats = CoinStatisticalData(other_coin,
                                                           asset_allocation,
                                                            forecasts,
                                                           standard_deviation_of_returns,
                                                           correlation).to_dict()
    
                    weight2 = other_coin_stats['weight']
                    standard_dev2 = other_coin_stats['standard_dev'] 

                    # add to portfolio variance
                    variance += self.calculate_correlated_coins_variance(weight, weight2,
                                                                         standard_dev, standard_dev2,
                                                                         covariance)
            # store expected return and variance
            result = MonteCarloResult(asset_allocation, total_return, variance)
            scenarios.append(result)

        return scenarios

    def calculate_coin_variance(self, weight, standard_dev):
        return (weight * standard_dev) ** 2

    def calculate_correlated_coins_variance(self, weight_coin1, weight_coin2, standard_dev_coin1,
                                            standard_dev_coin2, covariance):
        return weight_coin1 * weight_coin2 * \
               standard_dev_coin1 * standard_dev_coin2 * covariance

    def run(self, list_coins, number_scenarios, start_date, number_days,
            conf_interval, plot_scenarios=False, use_mock_data=False, mode='daily'):

        sim_started = datetime.datetime.now()
        self.logger.info("Starting simulation - {}".format(sim_started.strftime("%Y-%m-%d %H:%M:%S")))

        standard_deviation_of_returns, \
        correlation, forecasts = Forecaster(use_mock_data, confidence_interval=0.9)\
            .prepare_data_for_monte_carlo(list_coins,
                                          start_date,
                                          number_days,
                                          conf_interval,
                                          mode)

        scenarios = self.run_scenarios(number_scenarios, list_coins, forecasts,
                      standard_deviation_of_returns, correlation)

        returns, sharpes, variances = self.extract_info_from_scenarios(scenarios)

        if plot_scenarios:
            self.save_image_scenarios(list_coins,
                                      number_days,
                                      returns,
                                      scenarios,
                                      sharpes,
                                      variances)

        scenario_output_filename = 'scenarios.json'
        with open(scenario_output_filename, 'w') as f:
            json.dump(scenarios, f, cls=MonteCarloEncoder, indent=4)
            self.logger.debug("Exported file {}".format(scenario_output_filename))

        self.logger.info("Ended simulation - Time elapsed {}".format(datetime.datetime.now() - sim_started))
        return scenarios, forecasts

    def save_image_scenarios(self, list_coins, number_days, returns, scenarios, sharpes, variances):
        plt.figure(1, figsize=(12, 8))
        plt.subplot(121)
        plt.plot(returns, variances, 'o')
        plt.xlabel("Returns (USD)")
        plt.ylabel(r'Variance $\sigma^2 ({USD})^2$')
        plt.title("Expected returns x variance for coins {} after {} day(s)"
                  .format(list_coins, number_days))
        plt.grid()
        ###### Sharpe ratio plot
        plt.subplot(122)
        plt.title("Sharpe ratio of scenarios")
        plt.plot(numpy.arange(len(scenarios)), sharpes, 'o')
        plt.grid()
        plt.ylabel("Sharpe ratio")
        plt.xlabel("Scenario number")
        plt.tight_layout()
        plt.savefig('returns_vs_variance_{}.png'.format(
            datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

    def extract_info_from_scenarios(self, scenarios):
        returns, variances, sharpes = [], [], []
        for scenario in scenarios:
            returns.append(scenario.expected_return)
            variances.append(scenario.variance)
            sharpes.append(scenario.sharpe_ratio)
        return returns, sharpes, variances