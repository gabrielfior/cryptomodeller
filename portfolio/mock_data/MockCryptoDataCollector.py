import datetime
import os
import pandas as pd
import traceback

class MockCryptoDataCollector:

    def __init__(self):
        pass

    def fetch_historical_data(self, coin, mode):

        full_path = os.path.realpath(__file__)
        dirname = os.path.dirname(full_path)
        list_folders = [x[0] for x in os.walk(dirname)]
        list_folders.remove(dirname)
        available_modes = [i.split(os.sep)[-1] for i in list_folders]
        if mode not in available_modes:
            raise AttributeError("Mode not available, options: {}".format(available_modes))

        try:
            base_path = os.path.dirname(os.path.realpath(__file__))
            df = pd.read_csv(os.path.join(base_path, "{}/{}.csv".format(mode, coin)))
            # neglect hour-minute-second info
            df['timestamp'] = df['timestamp'].apply(lambda x: datetime.datetime.strptime(x,'%Y-%m-%d %H:%M:%S.%f').date())
            return df

        except Exception as inst:
            print("Could not find file {}.csv in folder {}".format(coin, base_path))
            traceback.print_exc()
            raise FileNotFoundError()

    def generate_data(self, mode, coin):

        if mode == 'daily':
            df = self.get_daily_price(coin)
            df['timestamp'] = df['timestamp'].apply(lambda x: datetime.datetime.strftime(x,"%Y-%m-%d 00:00:00.000"))
            df.to_csv(dir_path = os.path.dirname(os.path.realpath(__file__)) + '{}/{}.csv'.format(mode, coin))
        elif mode=='hourly':
            raise NotImplementedError("Implement hourly mock data generator")
        else:
            raise NotImplementedError("Mode {] not implemented".format(mode))


