from portfolio.Autoregression import AutoRegression
import matplotlib.pyplot as plt
import datetime
import pandas as pd

from portfolio.Forecaster import Forecaster


def plot_predictions(test_dataset, predictions, title):

    plt.plot(test_dataset.values, '--bo', label='expected')
    plt.plot(predictions, '--r', label='predicted')
    plt.title(title)
    plt.legend()
    plt.plot()


def increment_dataset_with_7_future_days(n_future_days : int) -> pd.DataFrame:

    # check if I should create dates or datetimes
    today = datetime.datetime.now()

    list_days = []
    for day_number in range(n_future_days):
        list_days.append(today + datetime.timedelta(days=day_number))

    test = pd.DataFrame(list_days, columns='timestamp')
    test = test.set_index('timestamp')
    return test


def transform_coins_into_trainset(coins:pd.DataFrame) -> pd.Series:

    coins = coins.set_index('timestamp')
    return coins.close


if __name__ == "__main__":

    ar = AutoRegression()

    # get train dataset
    coins = Forecaster(False, 0.9).fetch_historical_data_from_coin('NEO', mode='daily')

    train = transform_coins_into_trainset(coins)

    n_days = 7
    test = increment_dataset_with_7_future_days(n_days)

    ar.instantiate_model(train)

    predictions = ar.calculate_predictions(test)

    plot_predictions(test, predictions, 'NEO')


