import yaml
import os

class ConfigHandler:
    """
    Handler for portfolio config file
    """

    def __init__(self):
        self.config = self._read_config()
    
    def _read_config(self, filename = 'portfolio.yaml'):
        """
        Reads config file in path
        """

        dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(dir_path + os.sep + filename, 'r') as stream:
            config = yaml.load(stream)

        return config