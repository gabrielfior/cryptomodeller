import pandas as pd

class ScenarioManager:

    def __init__(self):
        pass

    @staticmethod
    def calculate_value_portfolio(scenario, forecasts, start_date, end_date):
        """
        Calculates return per day of scenario, given the asset allocation
        :param scenario:
        :param forecasts:
        :return:
        """
        if 'asset_allocation' not in scenario.__dict__.keys():
            raise KeyError('asset_allocation is required under scenario.__dict__.keys()')

        # get return per day of forecast
        list_coins = scenario.asset_allocation.keys()
        price_all_coins = pd.DataFrame()

        for coin in list_coins:

            # calculate return for coin and organize in days
            price_coin = forecasts[coin][(forecasts[coin].ds <= end_date) & (forecasts[coin].ds >= start_date)]
            price_coin = price_coin[['ds','yhat']].copy()
            price_coin['returns'] = price_coin.yhat.apply(lambda x: scenario.asset_allocation[coin] * x)

            price_all_coins = price_all_coins.append(price_coin)

        # get returns per day -> group by ds, sum returns
        return_calc = price_all_coins.groupby('ds', as_index=False).sum()

        return return_calc
