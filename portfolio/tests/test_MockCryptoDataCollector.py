# content of test_sample.py
import pytest
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
sys.path.append(os.path.abspath(os.path.join(os.pardir, '/mock_data')))
from portfolio.mock_data.MockCryptoDataCollector import MockCryptoDataCollector

def test_mock_crypto():

    # should find
    list_coins = ['ETH','IOT']
    for coin in list_coins:
        print(coin)
        mode = 'daily'
        df = MockCryptoDataCollector().fetch_historical_data(coin, mode=mode)
        assert df.empty == False

def test_mock_crypto_non_existent_coin():

    with pytest.raises(FileNotFoundError) as e_info:
        coin = 'OPS_I_DO_NOT_EXIST'
        mode = 'daily'
        df = MockCryptoDataCollector().fetch_historical_data(coin, mode=mode)
        # should have failed by now
        assert (False)

def test_mode_not_available():
    with pytest.raises(AttributeError, message="Expecting AttributeError") as mode_dont_exist_error:
        coin = 'ETH'
        mode = 'imaginary_mode'
        df = MockCryptoDataCollector().fetch_historical_data(coin, mode=mode)
        # should have failed by now
        assert (False)
