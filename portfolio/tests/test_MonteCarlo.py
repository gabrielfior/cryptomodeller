# content of test_sample.py
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
from MonteCarlo import MonteCarlo


def func(x):
    return x + 1

def test_monte_carlo():
    list_coins = ['ETH','XLM']
    number_days = 1
    confidence_interval = 0.9
    monte_carlo = MonteCarlo().run(list_coins, number_days, confidence_interval)

    #assert func(3) == 4