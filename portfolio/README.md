Goal is to create an intelligent portfolio
by finding the highest returns while keeping the
lowest possible risk.

# Run tests

1. Go to folder:
```
/Users/gabrielfior/CryptoModeller
```
2. Activate environment
```
source activate tensorflow
```
3. Run tests
```bash
python -m pytest portfolio/tests/
```