# train autoregression
from statsmodels.tsa.ar_model import AR
from sklearn.metrics import mean_squared_error


class AutoRegression:

    def __init__(self):
        pass

    def instantiate_model(self, train_dataset):
        self.model = AR(train_dataset)
        self.model_fit = self.model.fit()
        self.window = self.model_fit.k_ar
        self.history = train_dataset[len(train_dataset) - self.window:]
        self.history = [self.history[i] for i in range(len(self.history))]

    def calculate_predictions(self,test_dataset):

        predictions = list()
        for t in range(len(test_dataset)):
            length = len(self.history)
            lag = [self.history[i] for i in range(length - self.window, length)]
            yhat = self.model_fit.params[0]
            for d in range(self.window):
                yhat += self.model_fit.params[d + 1] * lag[self.window - d - 1]
            obs = test_dataset[t]
            predictions.append(yhat)
            self.history.append(obs)
            # print('predicted=%f, expected=%f' %(yhat, obs))

        error = mean_squared_error(test_dataset, predictions)
        print('Test MSE: %.3f' % error)

        return predictions

