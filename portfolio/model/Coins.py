"""Coin identifier"""
from enum import Enum


class Coins(Enum):

    ETHEREUM='ETH'
    BITCOIN='BTC'
    RIPPLE='XRP'
    BITCOIN_CASH='BCH'
    CARDANO='ADA'
    LITECOIN='LTC'
    STELLAR='XLM'
    NEM='XEM'
    NEO='NEO'
    IOTA='IOT'
    DASH='DASH'
    MONERO='XMR'
    BITCOIN_GOLD='BTG'
    QTUM='QTUM'
    ETHEREUM_CLASSIC='ETC'
    LISK='LSK'
    MOBIUS='MOBI'
