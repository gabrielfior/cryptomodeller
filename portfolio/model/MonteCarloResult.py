from json import JSONEncoder
import numpy as np

class MonteCarloResult:
    def __init__(self, asset_allocation, expected_return, variance):

        self.asset_allocation = asset_allocation
        self.expected_return = expected_return
        self.variance = variance
        self.sharpe_ratio = expected_return/np.sqrt(variance)

class MonteCarloEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__
