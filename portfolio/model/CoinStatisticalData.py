class CoinStatisticalData:

    def __init__(self, coin, asset_allocation, forecasts, standard_deviation, correlation_matrix):

        self.coin = coin
        self.weight = asset_allocation[coin]
        self.forecast = forecasts[coin].iloc[-1]
        self.standard_dev = standard_deviation[coin]

        # FIXME - pass suffix as configuration
        self.correlation_indices = correlation_matrix['close_' + coin]

    def to_dict(self):
        return self.__dict__


    def return_statistics(self, asset_allocation, predictions, standard_deviation, correlation_matrix):

        weight = asset_allocation[self.coin]
        forecast = predictions[self.coin]
        standard_dev = standard_deviation[self.coin]
        # FIXME - pass suffix as configuration
        correlation_indices = correlation_matrix['close_' + self.coin]
        #correlation_matrix['close_ETH']['close_XLM']

        return [weight, forecast, standard_dev, correlation_indices]
