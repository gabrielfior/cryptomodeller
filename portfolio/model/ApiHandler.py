import datetime

import requests
import pandas as pd
from LogHandler import LogHandler
from portfolio.config.ConfigHandler import ConfigHandler


class ApiHandler:

    def __init__(self):
        self.logger = LogHandler().create_logger()

        print("Testing connection")
        r = requests.get('https://httpbin.org/get')
        if r.status_code != requests.status_codes.codes.OK:
            raise Exception('Cannot fetch data. Status code was {}'.format(r.status_code))

        self.config = ConfigHandler()._read_config()

    @staticmethod
    def fetch_coin_list(self):
        """
        Fetches list of available coins from API
        :return: DataFrame containing available coins and additional info columns
        """
        url = self.config['urls']['coin_list']
        self.logger.info("Sending request to {}".format(url))
        page = requests.get(url)
        data = page.json()['Data']

        return pd.DataFrame.from_dict(data, orient='index')

    def hourly_price_historical(self, symbol, comparison_symbol, limit, aggregate, exchange=''):
        """
        Fetches daily historical data from coin
        Arguments:
            symbol {[str]} -- Currency for which data should be fetched
            comparison_symbol {[str]} -- Currency to compare to
            limit {[int]} -- [description]
            aggregate {[]} -- [description]

        Returns:
            [type] -- [DataFrame]
        """

        url = self.config['urls']['hourly_price'].format(symbol.upper(), comparison_symbol.upper(), limit, aggregate)
        if exchange:
            url += '&e={}'.format(exchange)

        self.logger.debug("Fetching hourly price from {}".format(url))
        page = requests.get(url)
        data = page.json()['Data']
        df = pd.DataFrame(data)
        # df = df[-10:]
        df['timestamp'] = [datetime.datetime.fromtimestamp(d) for d in df.time]
        return df

    def get_daily_price(self, coin):
        """"""
        url = self.config['urls']['daily_price'].format(coin)
        self.logger.info("Fetching daily price from {}".format(url))
        response_json = requests.get(url).json()
        self.logger.info("Days retrieved: {}".format(len(response_json['price'])))
        list_prices = []
        for price in response_json['price']:
            datetime_info = datetime.datetime.fromtimestamp(price[0]/1000)

            # neglect hourly info
            date = datetime_info.date()
            list_prices.append({'timestamp': date,
                                'close': price[1]})

        return pd.DataFrame(list_prices)