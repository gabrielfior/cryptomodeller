import datetime
import pprint
import pandas as pd
from portfolio.ScenarioManager import ScenarioManager
from portfolio.model.Coins import Coins
from portfolio.Forecaster import Forecaster
from portfolio.MonteCarlo import MonteCarlo

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def compare_actual_values(list_coins, scenario, current_date, use_mock_data):

    # get weights
    weights = scenario.asset_allocation
    # get actual prices of each coin in the date
    actual_quotes = {}
    actual_return = 0.
    price_all_coins = pd.DataFrame()
    for coin in list_coins:
        ############


        ##########

        df = get_prices_of_coins(coin, use_mock_data)
        df = df[(df.timestamp <= end_date.date()) & (df.timestamp >= start_date.date())]
        price = df.iloc[df.index.get_loc(current_date,
                                             method='nearest')]['close']

        price_coin = df[['timestamp', 'close']].copy()
        price_coin['returns'] = price_coin['close'].apply(lambda x: weights[coin] * x)

        price_all_coins = price_all_coins.append(price_coin)

        # get returns per day -> group by ds, sum returns
        return_calc = price_all_coins.groupby('timestamp', as_index=False).sum()

        actual_quotes[coin] = price
        actual_return += weights[coin] * price

    return {'actual_quotes': actual_quotes,
            'actual_return': actual_return,
            'dataframe_returns': return_calc}


def get_prices_of_coins(coin, use_mock_data):

    df = Forecaster(use_mock_data, confidence_interval=0.9).get_daily_price(coin)
    df = df.set_index(df['timestamp'])

    df = df.loc[~df.index.duplicated(keep='first')]

    return df


def pick_scenario(scenarios, mode):
    if mode=='return':
        # get highest return
        highest_return = 0.
        index_of_highest_return = None
        for scenario_count, scen in enumerate(scenarios):
            if scen.expected_return >= highest_return:
                highest_return = scen.expected_return
                index_of_highest_return = scenario_count

        return scenarios[index_of_highest_return]
    elif mode == 'sharpe_ratio':
        # get highest return
        highest_sharpe_ratio = 0.
        index_of_highest_sharpe_ratio = None
        for scenario_count, scen in enumerate(scenarios):
            if scen.sharpe_ratio >= highest_sharpe_ratio:
                highest_sharpe_ratio = scen.sharpe_ratio
                index_of_highest_sharpe_ratio = scenario_count

        return scenarios[index_of_highest_sharpe_ratio]

    else:
        raise NotImplementedError("not implemented for mode {}".format(mode))


if __name__ == "__main__":

    # start with 100 EUR
    investment = 100.

    list_coins = [Coins.ETHEREUM.value, Coins.CARDANO.value,
                  Coins.STELLAR.value, Coins.IOTA.value,
                  Coins.NEO.value]
    #list_coins = [Coins.ETHEREUM.value, Coins.CARDANO.value]

    # run Monte Carlo simulation
    print("Starting Monte Carlo simulation")
    number_days = 7
    use_mock_data = False
    #list_coins = ['XLM','ETH']
    confidence_interval = 0.9
    number_scenarios = 5000
    start_date = datetime.datetime.today() - datetime.timedelta(days=7)
    end_date = datetime.datetime.now()
    scenarios, forecasts = MonteCarlo().run(list_coins, number_scenarios, start_date,
                                   number_days, confidence_interval, plot_scenarios=False,
                                   use_mock_data=False, mode='daily'
                                   )

    # 1a. from scenario, get highest sharpe ratio and
    # 1b. calculate expected return after 1 week
    scenario = pick_scenario(scenarios, mode='sharpe_ratio')
    value_portfolio = ScenarioManager.calculate_value_portfolio(scenario, forecasts, start_date, end_date)
    pprint.pprint(scenario.__dict__)
    predicted_return = (scenario.expected_return - investment)/investment
    # 1c. Compare with real value from api
    current_date = datetime.datetime.today()
    dict_actual = compare_actual_values(list_coins, scenario, current_date, use_mock_data)
    actual_return = (dict_actual['actual_return'] - investment) / investment

    print('Expected: {a:8.2f}%, actual: {b:8.2f}%, diff: {c:8.2f}%'
          .format(a=predicted_return, b=actual_return, c= predicted_return - actual_return))

    # Fit 2 scenarios
    plt.figure(1, figsize=(12, 8))
    plt.subplot(111)
    plt.plot(value_portfolio.ds, value_portfolio.returns, '--ro')
    plt.xlabel("Datetime")
    plt.ylabel('Returns (USD)')
    plt.title("Expected returns for scenario")
    ##
    plt.plot(dict_actual['dataframe_returns'].timestamp,
             dict_actual['dataframe_returns'].returns, '--bo')

    plt.grid()
    plt.tight_layout()
    plt.savefig('return_evolution_{}.png'.format(
        datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

    print('debug')