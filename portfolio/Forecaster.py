import configparser
import os
import pandas as pd
import requests
import yaml
import matplotlib as mpl

from portfolio.mock_data.MockCryptoDataCollector import MockCryptoDataCollector
from portfolio.model.ApiHandler import ApiHandler

mpl.use('Agg')
import matplotlib.pyplot as plt
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))
from LogHandler import LogHandler

#import sys
#sys.path.append(os.path.dirname(__file__))

import datetime
from fbprophet import Prophet
from ConfigHandler import ConfigHandler


def plot_forecast(m, forecast):

    print(forecast.ds.max())
    fig = m.plot(forecast, xlabel='Date', ylabel='Price (USD)')
    plt.title('Price forecast {} (USD)'.format("unknown entity"))
    fig.gca().yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('${x:,.0f}'))

class Forecaster:

    def __init__(self, use_mock_data, confidence_interval):
        self.config = ConfigHandler().config
        self.use_mock_data = use_mock_data
        self.logger = LogHandler().create_logger()

    def fetch_historical_data_from_coin(self, coin, mode):

        supported_modes = ['daily','hourly']

        if mode=='daily':
            df = ApiHandler().get_daily_price(coin)
        elif mode == 'hourly':
            df = ApiHandler().hourly_price_historical(coin, 'USD', 9999, 1)
        else:
            raise NotImplementedError("Mode not implemented. Supported values: {}"
                                      .format(supported_modes))

        self.logger.info('Fetched data for {}'.format(coin))
        return df

    def pre_process_for_prophet(self, df):

        df['ds'] = df['timestamp']
        df['y'] = df['close']
        forecast_data = df[['ds', 'y']].copy()

        return forecast_data

    def forecast(self, df, n_days, confidence_interval, plot=False):

        # Create the Prophet model and fit the data
        forecast_data = self.pre_process_for_prophet(df)
        m = Prophet(interval_width=confidence_interval)
        m.fit(forecast_data)

        # predict
        future = m.make_future_dataframe(periods=n_days, freq='D')
        forecast = m.predict(future)

        if plot:
            plot_forecast(m, forecast)

        return forecast

    def calculate_returns_std(self, df):

        # calculate returns per hour
        df['returns'] = df.close.diff()
        df.dropna(how='any', inplace=True)
        return df.returns.std()

    def add_suffix_to_dataframe(self, df, coin):
        suffix = "_{}".format(coin)
        df = df.add_suffix(suffix)
        df.rename(columns={'timestamp' + suffix: 'timestamp'}, inplace=True)
        return df

    def delete_extra_columns(self, df):

        cols = ['volumeto', 'volumefrom', 'time',
                 'open', 'low', 'high']
        self.logger.debug("Deleting columns {}".format(cols))
        cols = [c for c in cols if c in df.index]

        df.drop(cols, axis=1,
                    inplace=True)

    def prepare_data_for_monte_carlo(self, list_coins, 
                                     start_date, number_days,
                                     confidence_interval, mode='daily'):

        # calculate predictions and return Dict : coin -> [yhat]
        standard_dev = {}
        predictions = {}
        forecasts = {}
        time_delta = 1 # aggregator
        dataframes_coins = pd.DataFrame()
        for coin in list_coins:

            if self.use_mock_data:
                df = MockCryptoDataCollector().fetch_historical_data(coin, mode)
            else:
                df = self.fetch_historical_data_from_coin(coin, mode=mode)

            self.delete_extra_columns(df)

            # filter by start date
            df = df[df.timestamp <= start_date.date()]

            # prediction
            forecast_coin = self.forecast(df, number_days, confidence_interval,
                                                       plot=False)

            predictions[coin] = forecast_coin.iloc[-1]
            forecasts[coin] = forecast_coin

            # calculate standard_deviation_of_returns : std[coin] = standard_dev
            standard_dev[coin] = self.calculate_returns_std(df)

            # add to general df
            df = self.add_suffix_to_dataframe(df, coin)
            if dataframes_coins.empty:
                dataframes_coins = df.copy()
            else:
                # merge
                dataframes_coins = dataframes_coins.merge(df, on='timestamp', how = 'outer')

        # calculate correlation : Correlation coefficient between coins : dict -> dict[coinA][coinB] = rho_A_B
        correlation = dataframes_coins.corr('pearson')

        return [standard_dev, correlation, forecasts]