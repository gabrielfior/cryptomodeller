import logging
import os
from logging.config import fileConfig

from ConfigEnums import ConfigEnums


class LogHandler:

    @staticmethod
    def create_logger():
        """
        Creates a rotating log
        """

        dir_path = os.path.dirname(os.path.realpath(__file__))
        fileConfig(dir_path + os.sep + ConfigEnums.CONFIG_FILEPATH.value)
        logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)
        logger = logging.getLogger()
        
        return logger
