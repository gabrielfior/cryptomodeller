import configparser
import os

from ConfigEnums import ConfigEnums


class ConfigHandler:
    """
    Handler for extracting config values
    """
    def __init__(self):
        self.config = configparser.ConfigParser()

        dir_path = os.path.dirname(os.path.realpath(__file__))

        self.config.read(dir_path + os.sep + ConfigEnums.CONFIG_FILEPATH.value)

    def get_value(self, section_name, key):

        """
        Returns value from config file from given section and key
        :param Section_name: Identifier of section
        :param key: Key from which value should be retrieved
        :return: Value associated to key
        """
        return self.config.get(section_name, key)

    def get_sections(self):
        return self.config.sections()